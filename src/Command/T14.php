<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class T14 extends Command
{
    const CLIENT_ID = '5df012b443551468e88e274cd10d3e16cd383e31';
    const CLIENT_SECRET = 'e467eea3bc9e2fa95d7f3760aff281867d0c4e02';
    private $container;

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:import:t14')
        ->setDescription('Import all the data.')
        ->setHelp("This command allows you to import all the data from t14");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $errOutput = $output instanceof ConsoleOutputInterface ? $output->getErrorOutput() : $output;
        $client = new Client(['base_uri' => 'https://api.turn14.com']);
        $headers = [ 'Content-Type' => 'application/json' ];

        $res = $client->request('POST', '/v1/token', [
            'json' => [
                'grant_type' => 'client_credentials',
                'client_id' => self::CLIENT_ID,
                'client_secret' => self::CLIENT_SECRET
            ]
        ]);
        $body = $res->getBody()->getContents();
        $credentials = json_decode(str_replace("\'", '\"', $body));
        $headers['Authorization'] = 'Bearer ' . $credentials->access_token;
        $page = 1;
        $npage;

        $progress = function ($page, $npage) {
            return chr(8) . chr(8) . chr(8) . chr(8) . round($page / $npage * 100) . '%';
        };

        do {
          $data = json_decode($client
            ->get('/v1/items?page=' . $page, [ 'headers' => $headers ])
            ->getBody()
            ->getContents());

          $npage = $data->meta->total_pages;
          $errOutput->write($progress($page, $npage));

          foreach ($data->data as $product) {
              $partNumber = $product->attributes->part_number;
              $output->writeln($partNumber);
          }

        } while (++$page <= $npage);
    }
}
