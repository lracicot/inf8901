<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class ImportGitHub extends Command
{
    private $container;
    private $clientId = '2d79171f08fec4600cf8';
    private $clientSecret = 'f79aa44b10c18ffeb6286a212d190e1700a412be';

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:import:repos')
        ->setDescription('Import all the repostories.')
        ->addOption('purge', null, InputOption::VALUE_NONE)
        ->addOption('ratelimit', null, InputOption::VALUE_NONE)
        ->addArgument(
            'ecosystem',
            InputArgument::REQUIRED
        )
        ->setHelp("This command allows you to import all the data from github");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ecosystem = $input->getArgument('ecosystem');

        if ($input->getOption('purge')) {
            $this->container['db']->executeUpdate('DELETE FROM repositories WHERE ecosystem = "' . $ecosystem . '"');
        }

        switch ($ecosystem) {
            case 'react':
                $query = 'created:"<+2016-01-01"+stars:>20+topic:react';
                break;
            case 'angular':
                $query = 'created:"<+2016-01-01"+stars:>20+topic:angular';
                break;
            case 'vue':
                $query = 'created:"<+2016-01-01"+stars:>20+topic:vue';
                break;
        }

        $client = new Client(['base_uri' => 'https://api.github.com/']);

        if ($input->getOption('ratelimit')) {
            $res = $client->get('/rate_limit?client_id='.$this->clientId.'&client_secret='.$this->clientSecret);
            $body = $res->getBody()->getContents();
            $data = json_decode(str_replace("\'", '\"', $body));
            var_dump($data);
            return;
        }

        $headers = [];
        $rawData = [];
        $prPromises = [];
        $page = 1;
        $npage = 0;


        // * * * * * * * * * * * *
        // 3. Map the data in the destination format
        // * * * * * * * * * * * *
        $map = function ($project) use ($ecosystem) {
            return (object) [
                'ghid' => $project->id,
                'name' => $project->name,
                'full_name' => $project->full_name,
                'clone_url' => $project->clone_url,
                'ecosystem' => $ecosystem,
                'verified' => 0,
            ];
        };

        // * * * * * * * * * * * *
        // 2. Transform the data
        // * * * * * * * * * * * *
        $transform = function (ResponseInterface $res) use (&$rawData, &$npage, $ecosystem, $map, $output) {
            $body = $res->getBody()->getContents();
            $entities = json_decode(str_replace("\'", '\"', $body));
            $npage = min(10, ceil($entities->total_count / 30));

            $rawData = ['data' => array_map($map, $entities->items), 'inserted' => false];
            $this->insert($rawData['data'], 'repositories', $output);
        };

        $handleException = function (\Exception $e) use ($output) {
            $output->writeln($e->getMessage());
        };

        // * * * * * * * * * * * *
        // 1. Retreive the data
        // * * * * * * * * * * * *
        $retreiveData = function () use (&$page, &$npage, &$prPromises, &$rawData, $query, $headers, $client, $handleException, $output, $transform) {
            do {
                $output->writeln('Getting data for page '.$page);
                $output->writeln('Expected number of pages: '.$npage);

                $url = '/search/repositories?page='.$page.'&per_page=100&q='.$query.'&client_id='.$this->clientId.'&client_secret='.$this->clientSecret;

                $prPromises[] = $client
                ->getAsync($url, $headers)
                ->then($transform, $handleException);
                sleep(2);

                Promise\unwrap($prPromises);
                Promise\settle($prPromises)->wait();
            } while (++$page <= $npage);
        };

        $retreiveData();

        // * * * * * * * * * * * *
        // 4. Insert the data in the destination
        // * * * * * * * * * * * *
    }

    public function insert($data, $table, $output)
    {
        foreach ($data as $toInsert) {
            $output->writeln('inserting data for repo #'.$toInsert->ghid);
            try {
                $this->container['db']->insert($table, (array)$toInsert);
            } catch (\Exception $e) {
                $output->writeln($e->getMessage());
            }
        }
    }
}
