<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Pool;
use _;

class FindQuarter extends Command
{
    private $container;

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:find:quarters')
        ->setDescription('Find the patterns')
        ->setHelp('Git gud');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commits = $this->container['db']->fetchAll('SELECT * FROM migrators m LEFT JOIN commits c on c.author = m.email ORDER BY c.author, c.date_authored');

        $quarters = [];

        foreach ($commits as $commit) {
            $commit = (object)$commit;
            $quarter = date('Y', strtotime($commit->date_authored)) . '-' . ceil(date('n', strtotime($commit->date_authored)) / 3);
            $quarters[$commit->author][$quarter][$commit->ecosystem] = 1;
        }

        foreach ($quarters as $developer => $dev_quarters) {
            foreach ($dev_quarters as $qnb => $quarter) {
                $this->insert([
                    'traveler' => $developer,
                    'quarter' => $qnb,
                    'eco' => implode(',', array_keys($quarter))
                ], 'quarter', $output);
            }
        }
    }

    public function insert($data, $table, $output)
    {
        var_dump($data);
        try {
            $this->container['db']->insert($table, (array)$data);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }
}
