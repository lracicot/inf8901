<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class T14Filter extends Command
{
    const QB_FILE = '/Users/master/Desktop/qb/Produits-Table 1.csv';
    const QB_OUTPUT = '/Users/master/Desktop/qb/import1.csv';
    const T14_FILE = '/Users/master/Desktop/pn.csv';

    private $container;

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:filtermfr:t14')
        ->setDescription('Filter all the data.')
        ->setHelp("This command allows you to filter all the data from t14");
    }

    private function compare($element, $t14Data)
    {
        $mfr_sku = strtolower(trim(substr($element, strpos($element, ' '))));
        var_dump($element);
        var_dump($mfr_sku);
        var_dump(array_filter($t14Data, function($t14_mfr) use ($mfr_sku) {
            return strpos($t14_mfr, $mfr_sku);
        }));
        var_dump('-----');
        return 0 < count(array_filter($t14Data, function($t14_mfr) use ($mfr_sku) {
            return strpos($t14_mfr, $mfr_sku);
        }));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // $output->writeln('Parsing T14 data');
        $t14Data = [];
        $handle = fopen(self::T14_FILE, 'r');
        while (($line = fgets($handle)) !== false) {
            $t14Data[] = $line;
        }
        fclose($handle);

        // $data = [];
        // $output->writeln('Parsing QB data');
        $file = fopen(self::QB_FILE, 'r');
        while (($line = fgetcsv($file)) !== FALSE) {
            if (strpos($line[3], '0912') !== false) {
                if ($this->compare($line[3], $t14Data)) {
                    // $output->writeln('"' . implode($line, '","') . '"');
                }
            }
        }
        fclose($file);
    }
}
