<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Pool;

class ImportCommits extends Command
{
    private $container;
    private $clientId = '2d79171f08fec4600cf8';
    private $clientSecret = 'f79aa44b10c18ffeb6286a212d190e1700a412be';

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:import:commits')
        ->setDescription('Import all the data.')
        ->addOption('purge', null, InputOption::VALUE_NONE)
        ->addOption('ratelimit', null, InputOption::VALUE_NONE)
        ->setHelp("This command allows you to import all the data from github");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = $this->container['http'];

        $prPromises = [];
        $page = 1;
        $npage = 0;


        // * * * * * * * * * * * *
        // 3. Map the data in the destination format
        // * * * * * * * * * * * *
        $map = function ($data) {
            try {
                return (object) [
                    'sha' => $data->sha,
                    'author' => $data->author->id,
                    'date_authored' => $data->commit->author->date,
                ];
            } catch (\Exception $e) {
                $this->container['log']->error($data->sha . ' : ' . $e->getMessage(), (array)$data);
            }
        };

        // * * * * * * * * * * * *
        // 2. Transform the data
        // * * * * * * * * * * * *
        $transform = function (ResponseInterface $res, $index) use (&$rawData, &$npage, $map, $output) {
            $body = $res->getBody()->getContents();
            $entities = json_decode(str_replace("\'", '\"', $body));

            if ($entities && is_array($entities)) {
                foreach ($entities as $entity) {
                    if (($data = [$map($entity)]) && !empty($data[0])) {
                        $this->insert($data, 'commits', $output);
                    }
                }
            }
        };

        $handleException = function (\Exception $e, $index) use ($output) {
            $output->writeln($e->getMessage());
        };

        $requests = function ($initial, $total, $repository) use ($output) {
            for ($i = $initial; $i <= $total; $i++) {
                $strIndex = str_pad($i, 4, ' ', STR_PAD_LEFT);
                $output->write(chr(8) . chr(8) . chr(8) . chr(8) . $strIndex);
                $url = substr($repository['commits_url'], 0, -6).'?page=' . $i .'&client_id='.$this->clientId.'&client_secret='.$this->clientSecret;
                yield new Request('GET', $url);
            }
        };

        $repositories = $this->container['db']->fetchAll('SELECT DISTINCT name, commits_url FROM repositories');

        foreach ($repositories as $repository) {
            $url = substr($repository['commits_url'], 0, -6).'?client_id='.$this->clientId.'&client_secret='.$this->clientSecret;
            $res = $client->get($url);
            $links = \GuzzleHttp\Psr7\parse_header($res->getHeader('Link'));
            $npage = (int)substr(current(array_filter($links, function($link) {
                return $link['rel'] == 'last';
            }))[0], -4);

            $output->writeln("\n" . 'Processing ' . $repository['name']);
            $output->write('Mapping ' . $npage . ' pages: ....');

            for ($i = 0; $i <= ceil($npage / 1000); $i++) {
                $poolId = $i * 100;
                $pool = new Pool($client, $requests($poolId, min($npage, 100 + $poolId), $repository), [
                    'concurrency' => 10,
                    'fulfilled' => $transform,
                    'rejected' => $handleException,
                ]);

                $promise = $pool->promise();
                $promise->wait();
            }
        }

        // $requests = [];
        //
        // // * * * * * * * * * * * *
        // // 1. Retreive the data
        // // * * * * * * * * * * * *
        // $retreiveData = function ($repository) use (&$page, &$npage, &$prPromises, &$rawData, &$requests, $client, $handleException, $output, $transform) {
        //     $url = substr($repository['commits_url'], 0, -6).'?client_id='.$this->clientId.'&client_secret='.$this->clientSecret;
        //     $res = $client->get($url);
        //     $links = \GuzzleHttp\Psr7\parse_header($res->getHeader('Link'));
        //     $npage = current(array_filter($links, function($link) {
        //         return $link['rel'] == 'last';
        //     }))[0];
        //
        //     for ($i = 0; $i <= $npage; $i++) {
        //         $requests[$i % 20][] = substr($repository['commits_url'], 0, -6).'?page=' . $i .'&client_id='.$this->clientId.'&client_secret='.$this->clientSecret;
        //         // $prPromises[] = $client
        //         // ->getAsync($url)
        //         // ->then($transform, $handleException);
        //         // sleep(2);
        //     }
        // };
        //
        // $repositories = $this->container['db']->fetchAll('SELECT DISTINCT commits_url FROM repositories');
        //
        // foreach ($repositories as $repository) {
        //     $retreiveData($repository);
        // }
        //
        // Promise\unwrap($prPromises);
        // Promise\settle($prPromises)->wait();

        // * * * * * * * * * * * *
        // 4. Insert the data in the destination
        // * * * * * * * * * * * *
    }

    public function insert($data, $table, $output)
    {
        foreach ($data as $toInsert) {
            try {
                $this->container['db']->insert($table, (array)$toInsert);
            } catch (\Exception $e) {
                $output->writeln($e->getMessage());
            }
        }
    }
}
