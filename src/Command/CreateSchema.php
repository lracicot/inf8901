<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateSchema extends Command
{
    private $container;

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
          ->setName('app:create:schema')
          ->setDescription('Create the schema if none exists.')
          ->addOption('force', null, InputOption::VALUE_NONE)
          ->setHelp("This command allows you to create the schema if it does not already exists.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sm = $this->container['db']->getSchemaManager();
        $schema = new \Doctrine\DBAL\Schema\Schema();

        if (count($sm->listTables())) {
            throw new \Exception('Tables already exists');
        }

        $pr = $schema->createTable('repositories');
        $pr->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $pr->addColumn('ghid', 'integer', ['unsigned' => true]);
        $pr->addColumn('name', 'string', ['length' => 255]);
        $pr->addColumn('full_name', 'string', ['length' => 255]);
        $pr->addColumn('clone_url', 'string', ['length' => 2048]);
        $pr->addColumn('ecosystem', 'string', ['length' => 255]);
        $pr->addColumn('verified', 'integer');
        $pr->setPrimaryKey(['id']);

        $pr = $schema->createTable('developer'); // author
        $pr->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $pr->addColumn('name', 'date', ['length' => 255]);
        $pr->addColumn('email', 'string', ['length' => 255]);
        $pr->setPrimaryKey(['id']);

        $pr = $schema->createTable('commits');
        $pr->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $pr->addColumn('repo', 'string', ['length' => 255]);
        $pr->addColumn('ecosystem', 'string', ['length' => 255]);
        $pr->addColumn('sha', 'string', ['length' => 255]);
        $pr->addColumn('author', 'string', ['length' => 255]);
        $pr->addColumn('commiter', 'string', ['length' => 255]);
        $pr->addColumn('date_authored', 'date');
        $pr->setPrimaryKey(['id']);

        $pr = $schema->createTable('issues');
        $pr->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $pr->addColumn('repo', 'string', ['length' => 255]);
        $pr->addColumn('ecosystem', 'string', ['length' => 255]);
        $pr->addColumn('ghid', 'string', ['length' => 255]);
        $pr->addColumn('author', 'string', ['length' => 255]);
        $pr->addColumn('date', 'date');
        $pr->setPrimaryKey(['id']);

        $pr = $schema->createTable('pull_requests');
        // $pr->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        // $pr->addColumn('repo', 'string', ['length' => 255]);
        // $pr->addColumn('ecosystem', 'string', ['length' => 255]);
        // $pr->addColumn('ghid', 'string', ['length' => 255]);
        // $pr->addColumn('author', 'string', ['length' => 255]);
        // $pr->addColumn('date', 'date');
        // $pr->setPrimaryKey(['id']);

        $pr = $schema->createTable('migrators');
        $pr->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $pr->addColumn('email', 'string', ['length' => 255]);
        $pr->addColumn('eco', 'string', ['length' => 255]);
        $pr->setPrimaryKey(['id']);

        foreach ($schema->toSql($sm->getDatabasePlatform()) as $query) {
            $this->container['db']->executeQuery($query);
        }
    }
}
