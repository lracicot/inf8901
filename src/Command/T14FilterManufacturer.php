<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class T14FilterManufacturer extends Command
{
    const QB_FILE = '/Users/master/Desktop/qb/Produits-Table 1.csv';
    const QB_OUTPUT = '/Users/master/Desktop/qb/import1.csv';
    const T14_FILE = '/Users/master/Desktop/pn.csv';

    private $container;

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:filter:mfr')
        ->setDescription('Filter all the data.')
        ->addArgument(
            'mfr',
            InputArgument::REQUIRED
        )
        ->setHelp("This command allows you to filter all the data from t14");
    }

    private function select($sku, $tla)
    {
        return strpos($sku, $tla) === 0;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tla = $input->getArgument('mfr');
        $file = fopen(self::QB_FILE, 'r');
        while (($line = fgetcsv($file)) !== FALSE) {
            if ($this->select($line[3], $tla)) {
                $output->writeln('"' . implode($line, '","') . '"');
            }
        }
        fclose($file);
    }

}
