<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Pool;
use _;

class FindData extends Command
{
    private $container;

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:find:data')
        ->setDescription('Find data')
        ->setHelp('Find data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $commits = [
            'vue' => $this->container['db']->fetchAll('SELECT DISTINCT author FROM commits WHERE ecosystem = "vue"'),
            'react' => $this->container['db']->fetchAll('SELECT DISTINCT author FROM commits WHERE ecosystem = "react"'),
            'angular' => $this->container['db']->fetchAll('SELECT DISTINCT author FROM commits WHERE ecosystem = "angular"'),
        ];

        $vra = array_intersect(
          array_map(function($c) { return $c['author']; }, $commits['vue']),
          array_map(function($c) { return $c['author']; }, $commits['angular']),
          array_map(function($c) { return $c['author']; }, $commits['react'])
        );

        $vr = array_diff(array_intersect(
          array_map(function($c) { return $c['author']; }, $commits['vue']),
          array_map(function($c) { return $c['author']; }, $commits['react'])
        ), $vra);

        $va = array_diff(array_intersect(
          array_map(function($c) { return $c['author']; }, $commits['vue']),
          array_map(function($c) { return $c['author']; }, $commits['angular'])
        ), $vra);

        $ra = array_diff(array_intersect(
          array_map(function($c) { return $c['author']; }, $commits['react']),
          array_map(function($c) { return $c['author']; }, $commits['angular'])
        ), $vra);

        foreach($vra as $migrator) {
          $this->insert([ 'email' => $migrator, 'eco' => 'vue,angular,react' ], 'migrators', $output);
        }

        foreach($vr as $migrator) {
          $this->insert([ 'email' => $migrator, 'eco' => 'vue,react' ], 'migrators', $output);
        }

        foreach($va as $migrator) {
          $this->insert([ 'email' => $migrator, 'eco' => 'vue,angular' ], 'migrators', $output);
        }

        foreach($ra as $migrator) {
          $this->insert([ 'email' => $migrator, 'eco' => 'angular,react' ], 'migrators', $output);
        }
    }

    public function insert($data, $table, $output)
    {
        try {
            $this->container['db']->insert($table, (array)$data);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }
}
