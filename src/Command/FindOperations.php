<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Pool;
use _;

class FindOperations extends Command
{
    private $container;

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:find:operations')
        ->setDescription('Find the operations')
        ->setHelp('Git gud');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container['db']->query('DELETE FROM operations');
        $quarters = $this->container['db']->fetchAll('SELECT * FROM quarter q ORDER BY q.traveler, q.quarter ASC');

        foreach ($quarters as $quarter) {
            $quarter = (object)$quarter;
            $lastQuarter = $this->findLastQuarter($quarter->traveler, $quarter->quarter);
            if (!$lastQuarter) {
                $this->insert([
                    'quarter' => $quarter->quarter,
                    'traveler' => $quarter->traveler,
                    'operation' => 'init',
                    'operand' => $quarter->eco,
                    'eco' => $quarter->eco,
                ], 'operations', $output);
            } else {
                $lastQuarter = (object)$lastQuarter;
                $thisEco = explode(',', $quarter->eco);
                $lastEco = explode(',', $lastQuarter->eco);

                $join = array_diff($thisEco, $lastEco);
                if (count($join)) {
                    foreach ($join as $eco) {
                        $this->insert([
                            'quarter' => $quarter->quarter,
                            'traveler' => $quarter->traveler,
                            'operation' => 'joins',
                            'operand' => $eco,
                            'eco' => $quarter->eco,
                        ], 'operations', $output);
                    }
                }

                $left = array_diff($lastEco, $thisEco);
                if (count($left)) {
                    foreach ($left as $eco) {
                        $this->insert([
                            'quarter' => $quarter->quarter,
                            'traveler' => $quarter->traveler,
                            'operation' => 'leaves',
                            'operand' => $eco,
                            'eco' => $quarter->eco,
                        ], 'operations', $output);
                    }
                }
            }
        }
    }

    private function findLastQuarter($contributor, $quarter) {
        // Check if already an operation
        $op = current($this->container['db']->fetchAll('SELECT * FROM operations o WHERE o.traveler = "' . $contributor . '" ORDER BY id DESC'));
        // var_dump($op);
        if ($op) {
            return [
                'traveler' => $op['traveler'],
                'quarter' => $op['quarter'],
                'eco' => $op['eco'],
            ];
        }

        if (substr($quarter, 5, 1) == '1') {
            $lastQuarter = (substr($quarter, 0, 4) - 1) . '-4';
        } else {
            $lastQuarter = (substr($quarter, 0, 5)) . (substr($quarter, 5, 1) - 1);
        }

        return current($this->container['db']->fetchAll('SELECT * FROM quarter q WHERE q.traveler = "' . $contributor . '" AND q.quarter = "' . $lastQuarter . '"'));
    }

    public function insert($data, $table, $output)
    {
        try {
            $this->container['db']->insert($table, (array)$data);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }
}
