<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Pool;
use _;

class FilterProjects extends Command
{
    private $container;

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:filter:projects')
        ->setDescription('Remove the leafs project from the ecosystem')
        ->setHelp('Remove the leafs project from the ecosystem');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repositories = $this->container['db']->fetchAll('SELECT DISTINCT id, name, full_name, clone_url FROM repositories');

        foreach ($repositories as $repository) {
            $output->writeln('Fetching npmjs data for ' . $repository['full_name']);
            $body = $this->container['http']->get('https://registry.npmjs.org/-/v1/search?text=' . $repository['name'])->getBody()->getContents();;
            $packages = json_decode(str_replace("\'", '\"', $body));

            $package = _::find($packages->objects, function ($package) use ($repository) {
                $repoUrl = 'https://github.com/' . $repository['full_name'];
                return isset($package->package->links->repository) && $package->package->links->repository === $repoUrl;
            });

            if ($package) {
                $this->container['db']->executeUpdate('UPDATE repositories SET verified = ? WHERE id = ?', array(1, $repository['id']));
            }
        }
    }
}
