<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Pool;
use _;

class FindPatterns extends Command
{
    private $container;

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:find:patterns')
        ->setDescription('Find the patterns')
        ->setHelp('Git gud');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container['db']->query('DELETE FROM patterns');
        $travelers = $this->container['db']->fetchAll('SELECT DISTINCT o.traveler FROM operations o');

        foreach ($travelers as $traveler) {
            $traveler = $traveler['traveler'];
            $operations = $this->container['db']->fetchAll('SELECT * FROM operations o WHERE o.traveler = "' . $traveler . '" ORDER BY o.quarter');

            $initialEcos = explode(',', $operations[0]['eco']);
            $finalEcos = explode(',', $operations[count($operations)-1]['eco']);

            $stays = array_intersect($initialEcos, $finalEcos);
            $left = array_diff($initialEcos, $finalEcos);
            $migrate = array_diff($finalEcos, $initialEcos);
            foreach ($stays as $eco) {
                $this->insert([
                    'traveler' => $traveler,
                    'quarter_from' => $operations[0]['quarter'],
                    'quarter_to' => $operations[count($operations)-1]['quarter'],
                    'role' => 'stayer',
                    'eco' => $eco,
                ], 'patterns', $output);
            }

            $migrated = [];
            if (!count($stays)) {
                foreach ($migrate as $eco) {
                    foreach ($operations as $operation) {
                        if ($operation['operation'] == 'joins' && in_array($operation['operand'], $migrate)) {
                            $migrated[$eco]['first_date'] = $operation['quarter'];
                        }
                        if ($operation['operation'] == 'leaves' && in_array($operation['operand'], $initialEcos)) {
                            $migrated[$eco]['last_date'] = $operation['quarter'];
                        }
                    }
                }
            }

            // It does not migrate, may have visited some ecos
            $visited = [];
            if (count($stays)) {
                foreach ($operations as $operation) {
                    if ($operation['operation'] == 'joins' && !in_array($operation['operand'], $stays)) {
                        $visited[$operation['operand']][] = $operation['quarter'];
                    }
                }
            }

            foreach ($visited as $eco => $quarters) {
                $this->insert([
                    'traveler' => $traveler,
                    'quarter_from' => $quarters[0],
                    'quarter_to' => $quarters[count($quarters)-1],
                    'role' => 'visitor',
                    'eco' => $eco,
                ], 'patterns', $output);
            }

            foreach ($migrated as $eco => $quarters) {
                $this->insert([
                    'traveler' => $traveler,
                    'quarter_from' => $quarters['first_date'],
                    'quarter_to' => $quarters['last_date'],
                    'role' => 'migrator',
                    'eco' => $eco,
                ], 'patterns', $output);
            }
        }
    }

    public function insert($data, $table, $output)
    {
        try {
            $this->container['db']->insert($table, (array)$data);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }
}
