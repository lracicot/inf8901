<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Pool;
use _;

class AnalyzeData extends Command
{
    private $container;

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:analyze:data')
        ->setDescription('Find the data')
        ->setHelp('Git gud');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $travelers = $this->container['db']->fetchAll('SELECT DISTINCT p.traveler FROM patterns p');
        $visitors = $this->container['db']->fetchAll('SELECT DISTINCT p.traveler FROM patterns p WHERE role = "visitor"');
        $migrators = $this->container['db']->fetchAll('SELECT DISTINCT p.traveler FROM patterns p WHERE role = "migrator"');

        $visitorStats = [
            'avg_visits_lgt' => [],
            'med_visits_lgt' => 0,
            'freq_visits' => [],
            'nb_commits_sec' => [],
            'nb_commits_first' => [],
            'entropies_first' => [],
            'entropies_sec' => [],
        ];

        $migratorStats = [
            'instant' => [],
            'overtime' => [],
            'migr_lgt' => [],
            'gap' => [],
            'entropies_first' => [],
            'entropies_sec' => [],
        ];

        // foreach ($visitors as $visitor) {
        //     var_dump($visitor['traveler']);
        //     $visitorStats['avg_visits_lgt'][] = $this->findAvgVisitLength($visitor['traveler']);
        //     $visitorStats['freq_visits'][] = $this->findVisitFreq($visitor['traveler']);
        //
        //     $changesFirst = [];
        //     $changesSec = [];
        //
        //     foreach ($this->findSecondaryEco($visitor['traveler']) as $eco) {
        //         $visitorStats['nb_commits_sec'][] = count($this->findCommits($visitor['traveler'], $eco));
        //
        //         foreach ($this->findCommits($visitor['traveler'], $eco) as $commit) {
        //             $changesSec[$eco . '/' . $commit['repo']][] = $this->getChanges($commit['sha'], $commit['repo'], $eco);
        //         }
        //     }
        //
        //     foreach ($this->getFirstEco($visitor['traveler']) as $eco) {
        //         $visitorStats['nb_commits_first'][] = count($this->findCommits($visitor['traveler'], $eco));
        //
        //         foreach ($this->findCommits($visitor['traveler'], $eco) as $commit) {
        //             $changesFirst[$eco . '/' . $commit['repo']][] = $this->getChanges($commit['sha'], $commit['repo'], $eco);
        //         }
        //     }
        //
        //     $entropiesFirst = [];
        //     $entropiesSec = [];
        //
        //     foreach ($changesFirst as $project => $allChanges) {
        //         $entropiesFirst[] = $this->calculateProjectEntropy($project, $allChanges);
        //     }
        //
        //     foreach ($changesSec as $project => $allChanges) {
        //         $entropiesSec[] = $this->calculateProjectEntropy($project, $allChanges);
        //     }
        //
        //     $visitorStats['entropies_first'][] = $this->avg(array_filter($entropiesFirst, function ($value) {
        //         return $value > 0 && $value < 1;
        //     }));
        //
        //     $visitorStats['entropies_sec'][] = $this->avg(array_filter($entropiesSec, function ($value) {
        //         return $value > 0 && $value < 1;
        //     }));
        // }
        //
        // $visitorStats['med_visits_lgt'] = $this->findMedVisitLength();
        //
        // $output->write('Average visit lenght: ');
        // $output->writeln(array_sum($visitorStats['avg_visits_lgt']) / count($visitorStats['avg_visits_lgt']));
        // $output->write('Average visit frequency: ');
        // $output->writeln(array_sum($visitorStats['freq_visits']) / count($visitorStats['freq_visits']));
        // $output->write('Median visit lenght: ');
        // $output->writeln($visitorStats['med_visits_lgt']);
        // $output->write('Average nb commits in sec eco: ');
        // $output->writeln(array_sum($visitorStats['nb_commits_sec']) / count($visitorStats['nb_commits_sec']));
        // $output->write('Average nb commits in main eco: ');
        // $output->writeln(array_sum($visitorStats['nb_commits_first']) / count($visitorStats['nb_commits_first']));
        // $output->write('Median nb commits in sec eco: ');
        // $output->writeln($visitorStats['nb_commits_sec'][ceil(count($visitorStats['nb_commits_sec'])/2)]);
        // $output->write('Median nb commits in main eco: ');
        // $output->writeln($visitorStats['nb_commits_first'][ceil(count($visitorStats['nb_commits_first'])/2)]);
        // $output->write('Avg entropy in main eco: ');
        // foreach ($visitorStats['entropies_first'] as $entropy) {
        //     $output->writeln($entropy);
        // }
        // $output->write('Avg entropy in sec eco: ');
        // foreach ($visitorStats['entropies_sec'] as $entropy) {
        //     $output->writeln($entropy);
        // }



        foreach ($migrators as $migrator) {
            var_dump($migrator['traveler']);

            if ($this->isInstantMigr($migrator['traveler'])) {
                $migratorStats['instant'][] = $migrator['traveler'];
                $migratorStats['gap'][] = $this->isGap($migrator['traveler']);

            } else {
                $migratorStats['overtime'][] = $migrator['traveler'];
                $migratorStats['migr_lgt'][] = $this->migrLength($migrator['traveler']);
            }

            // $changesFirst = [];
            // $changesSec = [];
            //
            // foreach ($this->findSecondaryEco($migrator['traveler']) as $eco) {
            //     foreach ($this->findCommits($migrator['traveler'], $eco) as $commit) {
            //         $changesSec[$eco . '/' . $commit['repo']][] = $this->getChanges($commit['sha'], $commit['repo'], $eco);
            //     }
            // }
            //
            // foreach ($this->getFirstEco($migrator['traveler']) as $eco) {
            //     foreach ($this->findCommits($migrator['traveler'], $eco) as $commit) {
            //         $changesFirst[$eco . '/' . $commit['repo']][] = $this->getChanges($commit['sha'], $commit['repo'], $eco);
            //     }
            // }
            //
            // $entropiesFirst = [];
            // $entropiesSec = [];
            //
            // foreach ($changesFirst as $project => $allChanges) {
            //     $entropiesFirst[] = $this->calculateProjectEntropy($project, $allChanges);
            // }
            //
            // foreach ($changesSec as $project => $allChanges) {
            //     $entropiesSec[] = $this->calculateProjectEntropy($project, $allChanges);
            // }
            //
            // $migratorStats['entropies_first'][] = $this->avg(array_filter($entropiesFirst, function ($value) {
            //     return $value > 0 && $value < 1;
            // }));
            //
            // $migratorStats['entropies_sec'][] = $this->avg(array_filter($entropiesSec, function ($value) {
            //     return $value > 0 && $value < 1;
            // }));
        }

        $output->write('Nb instant migr: ');
        $output->writeln(count($migratorStats['instant']));
        $output->write('Nb overtime migr: ');
        $output->writeln(count($migratorStats['overtime']));
        $output->write('Avg migr_lgt: ');
        foreach ($migratorStats['migr_lgt'] as $k => $lg) {
            if ($lg < 0) $migratorStats['migr_lgt'][$k] = $lg * -1;
        }
        $output->writeln($this->avg($migratorStats['migr_lgt']));
        foreach ($migratorStats['entropies_first'] as $entropy) {
            $output->writeln($entropy);
        }
        $output->write('Avg entropy in sec eco: ');
        foreach ($migratorStats['entropies_sec'] as $entropy) {
            $output->writeln($entropy);
        }
    }

    private function getFirstEco($traveler)
    {
        $ecos = current($this->container['db']->fetchAll('SELECT DISTINCT o.eco FROM operations o WHERE traveler = "' . $traveler . '" AND operation = "init"'));
        return explode(',', $ecos['eco']);
    }

    private function findSecondaryEco($traveler)
    {
        $firstEcos = $this->getFirstEco($traveler);
        $joins = $this->container['db']->fetchAll('SELECT DISTINCT o.operand FROM operations o WHERE traveler = "' . $traveler . '" AND operation = "joins"');
        $secondary = [];

        foreach ($joins as $eco) {
            if (!in_array($eco['operand'], $firstEcos)) {
                $secondary[] = $eco['operand'];
            }
        }

        return $secondary;
    }

    private function findAvgVisitLength($traveler)
    {
        $visits = $this->container['db']->fetchAll('SELECT quarter_from, quarter_to FROM patterns WHERE traveler = "' . $traveler . '" AND role = "visitor"');
        if (count($visits)) {
            $totalLength = 0;

            foreach ($visits as $visit) {
                $totalLength += $this->quarterToTime($visit['quarter_to']) - $this->quarterToTime($visit['quarter_from']) + 3600 * 24 * 30;
            }

            return $totalLength / count($visits) / 3600 / 24;
        }

        return 0;
    }

    private function findMedVisitLength()
    {
        $visits = $this->container['db']->fetchAll('SELECT quarter_from, quarter_to FROM patterns WHERE role = "visitor"');
        if (count($visits)) {
            $visitLenth = [];

            foreach ($visits as $visit) {
                $visitLenth[] = $this->quarterToTime($visit['quarter_to']) - $this->quarterToTime($visit['quarter_from']) + 3600 * 24 * 30;
            }

            return $visitLenth[ceil(count($visitLenth) / 2)] / 3600 / 24;
        }

        return 0;
    }

    private function findVisitFreq($traveler)
    {
        $visits = $this->container['db']->fetchAll('SELECT COUNT(*) as freq FROM operations WHERE traveler = "' . $traveler . '" AND operation = "joins"');
        return $visits[0]['freq'];
    }

    private function findCommits($traveler, $eco)
    {
        return $this->container['db']->fetchAll('SELECT * FROM commits WHERE author = "' . $traveler . '" AND ecosystem = "' . $eco . '"');
    }

    private function findMigrationLength($traveler)
    {
        $migrations = $this->container['db']->fetchAll('SELECT quarter_from, quarter_to FROM patterns WHERE traveler = "' . $traveler . '" AND role = "migrator"');
        if (count($migrations)) {
            $totalLength = 0;

            foreach ($migrations as $migration) {
                $totalLength += $this->quarterToTime($migration['quarter_to']) - $this->quarterToTime($migration['quarter_from']);
            }

            return $totalLength / count($migrations) / 3600 / 24;
        }

        return 0;
    }

    private function getChanges($sha, $repo, $eco)
    {
        $path = 'repos/' . $eco . '/' . $repo;
        $log =  exec('cd ' . $path . ' && git show ' . $sha . ' --numstat');

        $files = [];

        foreach (explode("\n", $log) as $line) {
            if (is_numeric(substr($line, 0, 1))) {
                $data = explode("\t", $line);
                $files[$data[2]] = $data[0] + $data[1];
            }
        }

        return $files;
    }

    private function getNbLines($project) {
        $path = 'repos/' . $project;
        $files = explode("\n", exec('cd ' . $path . ' && git ls-tree --full-tree -r HEAD | awk \'{print $2 " " $3}\''));

        $total = 0;

        foreach ($files as $file) {
            $total += (int)exec('cd ' . $path . ' && git cat-file ' . $file . ' | wc -l');
        }

        return $total;
    }

    private function calculateProjectEntropy($project, $allChanges)
    {
        $s_nb = 0;
        $s_lines = 0;
        $f_nb = [];
        $f_lines = [];

        foreach ($allChanges as $change) {
            $s_nb += count($change);
            $s_lines += array_sum($change);
            foreach ($change as $file => $lines) {
                if (!isset($f_nb[$file])) {
                    $f_nb[$file] = 0;
                }
                $f_nb[$file]++;
                if (!isset($f_lines[$file])) {
                    $f_lines[$file] = 0;
                }
                $f_lines[$file] += $lines;
            }
        }

        $H = 0;
        $nlines = $this->getNbLines($project);

        foreach ($f_lines as $file => $ch) {
            if ($s_nb > 0 && $s_lines > 0 && $nlines > 0) {
                $H += $f_nb[$file] / $s_nb * log($f_lines[$file] / $s_lines, $nlines);
            }
        }

        return $H * -1;
    }

    private function isInstantMigr($traveler)
    {
        $migr = $this->container['db']->fetchAll('SELECT * FROM patterns WHERE traveler = "' . $traveler . '" AND role = "migrator"');

        return $migr[0]['quarter_from'] == $migr[0]['quarter_to'];
    }

    private function isGap($traveler)
    {
        $lastJoin = $this->container['db']->fetchAll('SELECT quarter FROM operations WHERE traveler = "' . $traveler . '" AND operation = "joins" ORDER BY id DESC LIMIT 1');
        $lastLeave = $this->container['db']->fetchAll('SELECT quarter FROM operations WHERE traveler = "' . $traveler . '" AND operation = "leaves" ORDER BY id DESC LIMIT 1');

        return $lastJoin[0]['quarter'] != $lastLeave[0]['quarter'];
    }

    private function migrLength($traveler)
    {
        $migr = $this->container['db']->fetchAll('SELECT * FROM patterns WHERE traveler = "' . $traveler . '" AND role = "migrator"');

        return ($this->quarterToTime($migr[0]['quarter_to']) - $this->quarterToTime($migr[0]['quarter_from']) + 3600 * 24 * 30) / 3600 / 24;
    }


    private function avg($array) {
        if (count($array)) {
            return array_sum($array) / count($array);
        }

        return 0;
    }

    private function quarterToTime($quarter)
    {
        return strtotime(substr($quarter, 0, -1) . str_pad(substr($quarter, -1)*3, 2, '0', STR_PAD_LEFT) . '-01');
    }
}
