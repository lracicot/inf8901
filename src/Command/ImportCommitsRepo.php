<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Pool;

class ImportCommitsRepo extends Command
{
    private $container;
    private $clientId = '2d79171f08fec4600cf8';
    private $clientSecret = 'f79aa44b10c18ffeb6286a212d190e1700a412be';

    public function __construct($container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
        ->setName('app:import:commitrepos')
        ->setDescription('Import all the data.')
        ->addOption('purge', null, InputOption::VALUE_NONE)
        ->addOption('ratelimit', null, InputOption::VALUE_NONE)
        ->setHelp("This command allows you to import all the data from github");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repositories = $this->container['db']->fetchAll('SELECT DISTINCT ecosystem, clone_url, full_name FROM repositories WHERE verified = 1 ORDER BY full_name');

        foreach ($repositories as $repository) {

            $output->writeln('Processing ' . $repository['full_name']);
            $path = 'repos/' . $repository['ecosystem'] . '/' . $repository['full_name'];
            if (!file_exists($path)) {
                exec('git clone --bare ' . $repository['clone_url'] . ' ' . $path);
            }

            $map = function ($data) use ($repository) {
                return (object) [
                    'sha' => $data[0],
                    'repo' => $repository['full_name'],
                    'ecosystem' => $repository['ecosystem'],
                    'author' => $data[1],
                    'commiter' => $data[2],
                    'date_authored' => $data[3],
                ];
            };

            $commitsData = shell_exec('cd ' . $path . ' && git log --pretty=format:"%H,%ae,%ce,%ad" --date=short');
            $commits = array_map('str_getcsv', explode(PHP_EOL, $commitsData));
            $this->insert(array_map($map, $commits), 'commits', $output);
        }

        // * * * * * * * * * * * *
        // 4. Insert the data in the destination
        // * * * * * * * * * * * *
    }

    public function insert($data, $table, $output)
    {
        foreach ($data as $toInsert) {
            try {
                $this->container['db']->insert($table, (array)$toInsert);
            } catch (\Exception $e) {
                $output->writeln($e->getMessage());
            }
        }
    }
}
